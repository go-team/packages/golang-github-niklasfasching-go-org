golang-github-niklasfasching-go-org (1.7.0-1) unstable; urgency=medium

  * New upstream version 1.7.0
  * Remove 0001-Refresh-test-fixtures-for-chroma-v2.5.0.patch
    as it has been applied upstream.
    This reverts commit 9b4dd1fc82c395dcadff500b1f5db39ae75b8087.

 -- Anthony Fok <foka@debian.org>  Fri, 22 Sep 2023 09:20:32 -0600

golang-github-niklasfasching-go-org (1.6.6-1) unstable; urgency=medium

  * New upstream version 1.6.6
    - Fix race condition surrounding global orgWriter
  * Remove 0001-refresh-fixtures-for-chroma-v2.2.0.patch which has been
    merged upstream; see https://github.com/niklasfasching/go-org/pull/84
  * Add 0001-Refresh-test-fixtures-for-chroma-v2.5.0.patch
    as tabindex="0" was removed from <pre> elements in chroma v2.5.0;
    see https://github.com/niklasfasching/go-org/pull/100
  * Bump Standards-Version to 4.6.2 (no change)
  * Bump dependency: golang-github-alecthomas-chroma-v2-dev (>= 2.5.0)
  * Add symlink debian/missing-sources/blorg/testdata/public/about.html
    to ../../../../../blorg/testdata/content to resolve a Lintian
    source-is-missing warning.

 -- Anthony Fok <foka@debian.org>  Mon, 13 Mar 2023 15:14:49 -0600

golang-github-niklasfasching-go-org (1.6.5-1) unstable; urgency=medium

  * New upstream version 1.6.5
  * Update dependency on golang-github-alecthomas-chroma-v2-dev
    (replacing golang-github-alecthomas-chroma-dev) and on Go 1.18
    as per go.mod
  * Bump Standards-Version to 4.6.1 (no change)
  * Rename Built-Using field as Static-Built-Using
  * Refresh test fixtures for chroma v2.2.0 where missing semicolons
    have been added since chroma v2.0.1.  Bump dependency to
    golang-github-alecthomas-chroma-v2-dev (>= 2.2.0) accordingly.

 -- Anthony Fok <foka@debian.org>  Tue, 28 Jun 2022 06:45:49 -0600

golang-github-niklasfasching-go-org (1.6.2-1) unstable; urgency=medium

  * New upstream version 1.6.2
  * Remove debian/patches/0001-accommodate-chroma-0.9.2.patch
    as upstream now depends on chroma 0.10.0
  * Bump versioned dependencies as per go.mod

 -- Anthony Fok <foka@debian.org>  Sun, 20 Mar 2022 15:33:23 -0600

golang-github-niklasfasching-go-org (1.6.0-1) unstable; urgency=medium

  * New upstream version 1.6.0
  * Reorder fields in debian/control
    as would be generated in dh-make-golang >= 0.7.0
  * Use dh-sequence-golang instead of dh-golang and --with=golang

 -- Anthony Fok <foka@debian.org>  Thu, 13 Jan 2022 05:56:49 -0700

golang-github-niklasfasching-go-org (1.5.0-2) unstable; urgency=medium

  * Accommodate chrome v0.9.2 <pre> tabindex="0" attribute.
    This fixes FTBFS with chroma v0.9.2 and up in blorg/config_test.go
    while maintaining backward compatibility with older versions of chroma
    (Closes: #997560)
  * Add versions to dependent Go packages as per go.mod
  * Update Lintian overrides to match lintian 2.114.0 improved output
    which provides better file pointers for defective override files

 -- Anthony Fok <foka@debian.org>  Mon, 29 Nov 2021 01:44:08 -0700

golang-github-niklasfasching-go-org (1.5.0-1) unstable; urgency=medium

  * New upstream version 1.5.0
  * Bump build-dependency on golang-any to (>= 2:1.16~)
    as upstream began using //go:embed
  * Bump Standards-Version to 4.6.0 (no change)
  * Mark library package with "Multi-Arch: foreign"

 -- Anthony Fok <foka@debian.org>  Tue, 12 Oct 2021 04:24:41 -0600

golang-github-niklasfasching-go-org (1.4.0-1) unstable; urgency=medium

  * New upstream version 1.4.0
  * debian/control: Change Section from devel to golang
  * debian/control: Bump Standards-Version to 4.5.1 (no change)
  * debian/rules: Adjust DH_GOLANG_INSTALL_EXTRA for org/testdata/misc.org
    as upstream has changed from using .travis.yml to .github/workflows/ci.yml
    as test include file.

 -- Anthony Fok <foka@debian.org>  Tue, 02 Feb 2021 16:13:28 -0700

golang-github-niklasfasching-go-org (1.3.2-1) unstable; urgency=medium

  * New upstream version 1.3.2

 -- Anthony Fok <foka@debian.org>  Sat, 19 Sep 2020 01:43:40 -0600

golang-github-niklasfasching-go-org (1.3.0-1) unstable; urgency=medium

  * New upstream version 1.3.0
  * Bump debhelper dependency to "Build-Depends: debhelper-compat (= 13)"
  * Add debian/upstream/metadata file
  * debian/gbp.conf: Set "pristine-tar = True"

 -- Anthony Fok <foka@debian.org>  Tue, 14 Jul 2020 23:23:46 -0600

golang-github-niklasfasching-go-org (1.1.0-1) unstable; urgency=medium

  * New upstream version 1.1.0

 -- Anthony Fok <foka@debian.org>  Sat, 25 Apr 2020 19:49:36 -0600

golang-github-niklasfasching-go-org (1.0.0-1) unstable; urgency=medium

  * New upstream version 1.0.0

 -- Anthony Fok <foka@debian.org>  Fri, 21 Feb 2020 05:11:51 -0700

golang-github-niklasfasching-go-org (0.1.9-1) unstable; urgency=medium

  * New upstream version 0.1.9
  * Bump Standards-Version to 4.5.0 (no change)

 -- Anthony Fok <foka@debian.org>  Thu, 20 Feb 2020 00:43:21 -0700

golang-github-niklasfasching-go-org (0.1.8-2) unstable; urgency=medium

  * Source-only upload for migration into testing

 -- Anthony Fok <foka@debian.org>  Mon, 06 Jan 2020 03:16:23 -0700

golang-github-niklasfasching-go-org (0.1.8-1) unstable; urgency=medium

  * Initial release (Closes: #945998)

 -- Anthony Fok <foka@debian.org>  Tue, 03 Dec 2019 18:00:18 -0700
